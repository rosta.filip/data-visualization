import matplotlib.pyplot as plt

input_values = [0, 1, 2, 3, 4, 5]  # osa x -> její hodnoty, když nedám je to od 0, po 1
squares = [0, 1, 4, 9, 16, 25]  # osa y -> co chci vykreslit

plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(input_values, squares, linewidth=3)

# Set chart title and label axes.
ax.set_title('Square number', fontsize=24)
ax.set_xlabel('Value', fontsize=14)
ax.set_ylabel('Square of value', fontsize=14)

# Set size of tick labels.
ax.tick_params(axis='both', labelsize=14)

plt.show()
