import csv
from datetime import datetime

import matplotlib.pyplot as plt

filename = 'data/anchorage_weather_2021.csv'

"""
0 STATION
1 NAME
2 DATE
3 TAVG
4 TAVG_ATTRIBUTES
5 TMAX
6 TMAX_ATTRIBUTES
7 TMIN
8 TMIN_ATTRIBUTES
9 TOBS
10 TOBS_ATTRIBUTES
"""

with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    # Get dates, and high and low temperatures from this file.
    dates, highs, lows = [], [], []
    for row in reader:
        if row[1] == 'ANCHORAGE TED STEVENS INTERNATIONAL AIRPORT, AK US':
            current_day = datetime.strptime(row[2], '%Y-%m-%d')
            high = row[5]
            low = row[7]
            if high == '' or low == '':
                continue
            else:
                dates.append(current_day)
                highs.append(float(high))
                lows.append(float(low))

# Plot the high temperatures.
plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(dates, highs, c='red', alpha=0.5)
ax.plot(dates, lows, c='blue', alpha=0.5)
ax.fill_between(dates, highs, lows, facecolor='black', alpha=0.1)

# Format plot.
ax.set_title("Daily high and low temperatures in 2021\nAnchorage, AK", fontsize=20)
ax.set_xlabel('', fontsize=14)
fig.autofmt_xdate()
ax.set_ylabel('Temperature (Celsius)', fontsize=14)
ax.tick_params(axis='both', which='major', labelsize=14)

plt.show()

