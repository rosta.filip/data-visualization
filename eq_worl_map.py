import json
from datetime import datetime

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline

filename = 'data/eq_4.5_month.json'
with open(filename) as f:
    all_eq_data = json.load(f)

meta_data = all_eq_data['metadata']
high_title = meta_data['title']
end_date = datetime.utcfromtimestamp((meta_data['generated'])//1000)

all_eq_dicts = all_eq_data['features']

# ____ for better understanding...
# mags, lons, lats, hover_text = [], [], [], []
# for eq_dict in all_eq_dicts:
#    mag = eq_dict['properties']['mag']
#    lon = eq_dict['geometry']['coordinates'][0]
#    lat = eq_dict['geometry']['coordinates'][1]
#    title = eq_dict['properties']['title']
#    date = eq_dict['properties']['time']
#    mags.append(mag)
#    lons.append(lon)
#    lats.append(lat)
#    hover_text.append(f'{title} ({datetime.utcfromtimestamp(date//1000)})')

# ___ for better code ()
mags = [eq_dict['properties']['mag'] for eq_dict in all_eq_dicts]
lons = [eq_dict['geometry']['coordinates'][0] for eq_dict in all_eq_dicts]
lats = [eq_dict['geometry']['coordinates'][1] for eq_dict in all_eq_dicts]
hover_text = [f"{(eq_dict['properties']['title'])} ({(datetime.utcfromtimestamp(eq_dict['properties']['time']//1000))})"
         for eq_dict in all_eq_dicts]

# Map the earthquakes.
# data = [Scattergeo(lon=lons, lat=lats)] -> simpliest way, not good, if you want to customize data presentation.
data = [{
    'type': 'scattergeo',
    'lon': lons,
    'lat': lats,
    'text': hover_text,
    'marker': {
        'size': [3 * mag for mag in mags],
        'color': mags,
        'colorscale': 'Viridis',
        'reversescale': True,
        'colorbar': {'title': 'Magnitude'},
    },
}]

my_layout = Layout(title=f'{high_title}\n({end_date})')
fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='global_earthquakes.html')
