from operator import itemgetter

import requests
import json

import time

# https://en.wikipedia.org/wiki/Unix_time -> call with unix time
unix_time = int(time.time())

# Make an API call and store the response.
url = f'https://hacker-news.firebaseio.com/v0/topstories.json'
r = requests.get(url)
print(f"Status code: {r.status_code}")

# Process information about each submission.
submission_ids = r.json()
print(submission_ids)
submission_dicts = []

for submission_id in submission_ids[:40]:
    # Make a separate API call for each submission,
    url = f'https://hacker-news.firebaseio.com/v0/item/{submission_id}.json'
    r = requests.get(url)
    print(f"ID: {submission_id}\t status code: {r.status_code}")
    response_dict = r.json()

    # Build a dictionary for each article (Try if URL is inside, looking for a 'story').
    try:
        response_dict['url']
    except KeyError:
        if response_dict['type'] == 'story':
            submission_dict = {
                'title': response_dict['title'],
                'hn_link': f"http://news.ycombinator.com/item?id={submission_id}",
                'comments': response_dict['descendants'],
                'title_url': 'No url link.',
            }
            submission_dicts.append(submission_dict)
        else:
            continue
    else:
        if response_dict['type'] == 'story':
            submission_dict = {
                'title': response_dict['title'],
                'hn_link': f"http://news.ycombinator.com/item?id={submission_id}",
                'comments': response_dict['descendants'],
                'title_url': response_dict['url'],
            }
            submission_dicts.append(submission_dict)
        else:
            continue

submission_dicts = sorted(submission_dicts, key=itemgetter('comments'), reverse=True)

for submission_dict in submission_dicts:
    print(f"\nTitle: {submission_dict['title']}")
    print(f"Title link: {submission_dict['title_url']}")
    print(f"Discussion link: {submission_dict['hn_link']}")
    print(f"Comments: {submission_dict['comments']}")
