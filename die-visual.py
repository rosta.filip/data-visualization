from plotly.graph_objs import Bar, Layout
from plotly import offline

from die import Die

#_____________________________ ONE D6 die_____________________
# Create two D6 (6-sided die) dice
die = Die()

# Make some rolls, and store a result.
results = []
for roll_num in range(50000):
    results.append(die.roll())

# Analyze results.
frequencies = []
for value in range(1, die.num_sides+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Visualize the results.
x_values = list(range(1, die.num_sides+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title': 'Result'}
y_axis_config = {'title': 'Frequencies'}
my_layout = Layout(title='Result of rolling one D6 50.000 times.', xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d6.html')

#__________________two D6 dice__________________
# Create two D6 (6-sided die) dice
die_1 = Die()
die_2 = Die()

# Make some rolls, and store a result.
results = []
for roll_num in range(50000):
    results.append(die_1.roll() + die_2.roll())

# Analyze results.
frequencies = []
max_result = die_1.num_sides + die_2.num_sides
for value in range(2, max_result+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Visualize the results.
x_values = list(range(2, max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequencies'}
my_layout = Layout(title='Result of rolling two D6 dice 50.000 times.', xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d6_d6.html')


#__________________one D6 die and one D10 die__________________
# Create one D6 (6-sided die)die and one D10 die
die_6 = Die()
die_10 = Die(10)

# Make some rolls, and store a result.
results = []
for roll_num in range(50000):
    results.append(die_6.roll() + die_10.roll())

# Analyze results.
frequencies = []
max_result = die_6.num_sides + die_10.num_sides
for value in range(2, max_result+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Visualize the results.
x_values = list(range(2, max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequencies'}
my_layout = Layout(title='Result of rolling one D6 die and one D10 die 50.000 times.', xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d6_d10.html')


#__________________two D8 dice__________________
# Create two D8 (8-sided die) dice
die_1 = Die(8)
die_2 = Die(8)

# Make some rolls, and store a result.
results = []
for roll_num in range(50000):
    results.append(die_1.roll() + die_2.roll())

# Analyze results.
frequencies = []
max_result = die_1.num_sides + die_2.num_sides
for value in range(2, max_result+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Visualize the results.
x_values = list(range(2, max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequencies'}
my_layout = Layout(title='Result of rolling two D8 dice 50000 times.', xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d8_d8.html')


#__________________three D6 dice__________________
# Create three D6 (6-sided die) dice
die_1 = Die()
die_2 = Die()
die_3 = Die()

# Make some rolls, and store a result.
# -> list comprehension
results = [die_1.roll() + die_2.roll() + die_3.roll() for roll_num in range(50000)]

# Analyze results.
max_result = die_1.num_sides + die_2.num_sides + die_3.num_sides
frequencies = [results.count(value) for value in range(3, max_result+1)]

# Visualize the results.
x_values = list(range(3, max_result+1))
data = [Bar(x=x_values, y=frequencies)]

x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequencies'}
my_layout = Layout(title='Result of rolling three D6 dice 50000 times.', xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d6_d6_d6.html')
