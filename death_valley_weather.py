import csv
from datetime import datetime

import matplotlib.pyplot as plt

filename = 'data/death_valley_2021.csv'

"""
0 "STATION"
1 "NAME"
2 "DATE"
3 "TMAX"
4 "TMIN"
5 "TOBS"
"""

with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    # Get dates, and high and low temperatures from this file.
    dates, highs, lows = [], [], []
    for row in reader:
        current_day = datetime.strptime(row[2], '%Y-%m-%d')
        try:
            high = float(row[3])
            low = float(row[4])
        except ValueError:
            print(f'Missing data for {current_day}.')
        else:
            dates.append(current_day)
            highs.append(high)
            lows.append(low)

# Plot the high temperatures.
plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(dates, highs, c='red', alpha=0.5)
ax.plot(dates, lows, c='blue', alpha=0.5)
ax.fill_between(dates, highs, lows, facecolor='black', alpha=0.1)

# Format plot.
ax.set_title("Daily high and low temperatures in 2021\nDeath Valley, CA", fontsize=20)
ax.set_xlabel('', fontsize=14)
fig.autofmt_xdate()
ax.set_ylabel('Temperature (Celsius)', fontsize=14)
ax.tick_params(axis='both', which='major', labelsize=14)

plt.show()

